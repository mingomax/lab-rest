<?php
/**
 * @param $string
 * @param $limit
 * @param string $pad
 * @return string
 *
 * A single function to test Xdebug works.
 */
function truncate_str($string, $limit, $pad="..."){
    if(strlen($string) > $limit){
        $string = substr($string, 0, $limit-strlen($pad)).$pad;
    }
    return $string;
}
echo truncate_str("this string really needs to be truncated", 20);